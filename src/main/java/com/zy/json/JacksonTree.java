package com.zy.json;

import com.zy.pojo.Student;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;

public class JacksonTree {
    //json-----> tree
    private void analysisJson(String jsonString) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            //将json解析为一个树形结构，加载到内存
            JsonNode jsonNode = mapper.readTree(jsonString);

            //解析节点
            JsonNode nameNode = jsonNode.path("name");
            System.out.println("Name" + nameNode.getTextValue());  //string 类型

            JsonNode ageNode = jsonNode.path("age");
            System.out.println("Age:" + ageNode.getIntValue());  //int 类型

            JsonNode verifiedNode = jsonNode.path("verified");  //boolean
            System.out.println("Verified: " + (verifiedNode.getBooleanValue() ? "Yes" : "No"));

            JsonNode marksNode = jsonNode.path("marks");  //list类型
            Iterator<JsonNode> elements = marksNode.getElements();
            System.out.println("Marks:[");
            while (elements.hasNext()) {
                JsonNode markNode = elements.next();
                System.out.println(markNode.getIntValue());
            }
            System.out.println("]");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //tree-----> object
    private Student treeToObj() {
        Student stu=null;
        ObjectMapper mapper = new ObjectMapper();
        //创建一个对象类型的Node(树根)
        ObjectNode objectNode = mapper.createObjectNode();
        //创建一个数组类型的Node
        ArrayNode arrayNode = mapper.createArrayNode();
        arrayNode.add(100);
        arrayNode.add(90);
        arrayNode.add(85);
        objectNode.put("name", "Mahesh Kumar");
        objectNode.put("age", 18);
        objectNode.put("verified", false);
        objectNode.put("marks", arrayNode);


        try {
            String json = mapper.writeValueAsString(objectNode);
            System.out.println(json);
            JsonNode rootNode = mapper.readTree(json);
            stu=mapper.treeToValue(rootNode, Student.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stu;
    }

    public static void main(String[] args) {
        JacksonTree jt = new JacksonTree();
        String jsonString = "{\"name\":\"Mahesh Kumar\", \"age\":21,\"verified\":false,\"marks\": [100,90,85]}";
        jt.analysisJson(jsonString);
        Student student=jt.treeToObj();
        System.out.println("Name: "+ student.getName());
        System.out.println("Age: " + student.getAge());
        System.out.println("Verified: " + (student.isVerified() ? "Yes":"No"));
        System.out.println("Marks: "+Arrays.toString(student.getMarks()));
    }
}
