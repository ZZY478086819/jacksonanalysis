package com.zy.json;

import com.zy.pojo.Student;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import java.io.IOException;

/**
 * 将json字符串，转化为一个java对象
 */
public class JacksonTester01 {
    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();
        String json = "{\"name\":\"Mahesh\", \"age\":21}";

        try {
            //map json to student
            Student student=mapper.readValue(json,Student.class);
            System.out.println(student);

            //map student to json
            mapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);
            //
            String stuJson=mapper.writeValueAsString(student);
            System.out.println(stuJson);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
