package com.zy.json;

import com.zy.pojo.Student;
import org.codehaus.jackson.*;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class JsonGenerator01 {
    //构建json串，并写入文件
    public void buildJsonToFile(){
        JsonFactory factory=new JsonFactory();
        try {
            //基于json文件，创建一个JsonGenerator对象
            JsonGenerator jsonGenerator =  factory.createJsonGenerator(new File("student.json"),
                    JsonEncoding.UTF8);

            jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("name","Mahesh Kumar");
            jsonGenerator.writeNumberField("age", 21);
            jsonGenerator.writeBooleanField("verified", false);
            jsonGenerator.writeFieldName("marks");
            jsonGenerator.writeStartArray();
            jsonGenerator.writeNumber(100);
            jsonGenerator.writeNumber(90);
            jsonGenerator.writeNumber(85);
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
            jsonGenerator.close();

            ObjectMapper mapper = new ObjectMapper();
            //这里也可以转换成Map
            Student stu = mapper.readValue(new File("student.json"), Student.class);
            System.out.println(stu);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void readJsonToFile(){
        JsonFactory jasonFactory = new JsonFactory();
        try {
            JsonParser jsonParser = jasonFactory.createJsonParser(new File("student.json"));
            while(jsonParser.nextToken()!= JsonToken.END_OBJECT){
                String fieldName=jsonParser.getCurrentName();
                if("name".equals(fieldName)){
                    //move to next token
                    jsonParser.nextToken();
                    System.out.println(jsonParser.getText());
                }
                if("age".equals(fieldName)){
                    //move to next token
                    jsonParser.nextToken();
                    System.out.println(jsonParser.getNumberValue());
                }
                if("verified".equals(fieldName)){
                    //move to next token
                    jsonParser.nextToken();
                    System.out.println(jsonParser.getBooleanValue());
                }
                if("marks".equals(fieldName)){
                    //move to [
                    jsonParser.nextToken();
                    while(jsonParser.nextToken()!=JsonToken.END_ARRAY){
                        System.out.println(jsonParser.getNumberValue());
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
     new JsonGenerator01().readJsonToFile();
    }
}
