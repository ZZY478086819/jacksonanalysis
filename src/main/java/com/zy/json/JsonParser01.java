package com.zy.json;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

public class JsonParser01 {
    public static void main(String[] args) {
        String jsonString = "{\"id\":1, \"name\":\"lzj\", \"cars\":[\"audi\", \"baoma\", \"benci\"]}";
        ObjectMapper mapper=new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER,true);
        try {
            JsonNode node=mapper.readTree(jsonString);
            if(node.isObject()){
                 Iterator<Map.Entry<String, JsonNode>> fields = node.getFields();
                 while(fields.hasNext()){
                     Map.Entry<String, JsonNode> next = fields.next();
                     String key=next.getKey();
                     JsonNode value=next.getValue();
                     System.out.println("name:"+next.getKey());
                     if(value.isTextual()){
                         System.out.println(value.getTextValue());
                     }
                     if(value.isInt()){
                         System.out.println(value.getNumberValue());
                     }
                     if(value.isArray()){
                         Iterator<JsonNode> iterator = value.iterator();
                         while(iterator.hasNext()){
                              JsonNode next1 = iterator.next();
                             System.out.println(next1.getTextValue());
                         }
                     }
                 }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
