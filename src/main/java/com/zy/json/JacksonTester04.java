package com.zy.json;

import com.zy.pojo.Student;
import com.zy.pojo.UserData;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.*;
import java.io.File;

public class JacksonTester04<T> {
    //list<Object> 类型的json,转化为对象
    private List<T> readListObject(ObjectMapper mapper, String json, TypeReference<List<T>> ref) {
        List<T> list = null;
        try {
            list = mapper.readValue(json, ref);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    //Map<String,Object> 类型的json,转化为对象
    private Map<String, T> readMapObject(ObjectMapper mapper, String json, TypeReference<Map<String, T>> ref) {
        Map<String, T> userDataMap = null;
        try {
            userDataMap = mapper.readValue(json, ref);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userDataMap;
    }

    public static void main(String[] args) {
        JacksonTester04 test = new JacksonTester04<>();
        ObjectMapper mapper = new ObjectMapper();
        Student student1 = new Student();
        student1.setAge(10);
        student1.setName("Mahesh");
        Student student2 = new Student();
        student2.setAge(11);
        student2.setName("ZY");
        List<Student> listPerson = new ArrayList<>();
        listPerson.add(student1);
        listPerson.add(student2);
        try {
            String stuJson = mapper.writeValueAsString(listPerson);
            List<Student> students = test.readListObject(mapper, stuJson,
                    new TypeReference<List<Student>>() {
                    });
            for (Student s : students) {
                System.out.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, UserData> userDataMap = new HashMap<>();
        UserData studentData = new UserData();

        int[] marks = {1, 2, 3};
        Student student = new Student();
        student.setAge(10);
        student.setName("Mahesh");

        // JAVA Object
        studentData.setStudent(student);
        // JAVA String
        studentData.setName("Mahesh Kumar");
        // JAVA Boolean
        studentData.setVerified(Boolean.FALSE);
        // Array
        studentData.setMarks(marks);
        userDataMap.put("studentData1", studentData);

        try {
            String json=mapper.writeValueAsString(userDataMap);
            userDataMap=test.readMapObject(mapper,json,
                    new TypeReference<Map<String, UserData>>(){});
            System.out.println(userDataMap.get("studentData1").getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
