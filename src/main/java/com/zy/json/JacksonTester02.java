package com.zy.json;

import com.zy.pojo.Student;
import org.codehaus.jackson.map.ObjectMapper;
import java.io.File;
import java.io.IOException;

public class JacksonTester02 {
    public static void main(String[] args) {
        JacksonTester02 test=new JacksonTester02();
        Student student = new Student();
        student.setAge(10);
        student.setName("Mahesh");
        try {
            test.writeJson(student);
            Student stu =test.readJSON();
            System.out.println(stu);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //将对象变成一个json串，写入文件
    private void writeJson(Student student) throws IOException {
        String fileName="student.json";
        File file=new File("student.json");
        if(!file.exists()){
            file.createNewFile();
        }
        ObjectMapper mapper=new ObjectMapper();
        mapper.writeValue(file,student);
    }
    //读取文件中的json，转换成对象
    private Student readJSON() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Student stu= mapper.readValue(new File("student.json"),Student.class);
        return stu;
    }
}
